// ==UserScript==
// @name         FastTextComment
// @namespace    padadas
// @version      1.6
// @comment      Szybkie komentarze do  zgłoszenia w Jirze
// @author       Patryk Dadas (padadas)
// @match        https://*jira*/browse/*
// @grant        unsafeWindow
// @require      http://code.jquery.com/jquery-latest.js
// @require      https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js
// @resource     https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css
// @grant        GM_addStyle
// @resource     https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css
// @grant        GM_getResourceText
// ==/UserScript==

function ChangeIds()
{

    var ileRekordow = document.getElementsByClassName("rowComment").length;

    for (i=1;i<=ileRekordow;i++)

    {
        document.getElementsByClassName("rowComment")[i-1].setAttribute("id","row"+i);
        document.getElementsByClassName("nameFastTextComment_rowComment")[i-1].setAttribute("id","nameFastTextComment_row"+i);
        document.getElementsByClassName("sourceFastTextComment_rowComment")[i-1].setAttribute("id","sourceFastTextComment_row"+i);
        document.getElementsByClassName("Commentedit")[i-1].setAttribute("id","edit_buttonComment"+i);
        document.getElementsByClassName("Commentsave")[i-1].setAttribute("id","save_buttonComment"+i);
        document.getElementsByClassName("Commentdelete")[i-1].setAttribute("id","delete_button"+i);
    }
}


function edit_rowComment(noComment)
{
    noComment = noComment.replace(/[a-zA-Z_]/g, '');
    document.getElementById("edit_buttonComment"+noComment).style.display="none";
    document.getElementById("save_buttonComment"+noComment).style.display="block";

    var nameFastTextComment=document.getElementById("nameFastTextComment_row"+noComment);
    var sourceFastTextComment=document.getElementById("sourceFastTextComment_row"+noComment);

    var nameFastTextComment_data=nameFastTextComment.innerHTML;
    var sourceFastTextComment_data=sourceFastTextComment.innerHTML;

    nameFastTextComment.innerHTML="<input type='text' id='nameFastTextComment_text"+noComment+"' value='"+nameFastTextComment_data+"'>";
    sourceFastTextComment.innerHTML="<textarea id='sourceFastTextComment_text"+noComment+"'>"+sourceFastTextComment_data+"</textarea>";
}

function save_rowComment(noComments)
{
    noComments = noComments.replace(/[a-zA-Z_]/g, '');
    var nameFastTextComment_val=document.getElementById("nameFastTextComment_text"+noComments).value;
    var sourceFastTextComment_val=document.getElementById("sourceFastTextComment_text"+noComments).value;

    document.getElementById("nameFastTextComment_row"+noComments).innerHTML=nameFastTextComment_val;
    document.getElementById("sourceFastTextComment_row"+noComments).innerHTML=sourceFastTextComment_val;
    document.getElementById("edit_buttonComment"+noComments).style.display="block";
    document.getElementById("save_buttonComment"+noComments).style.display="none";
    SaveCommentSnipettsToLocalStorage();

}

function delete_rowComment(noCommentsss)
{
    noCommentsss = noCommentsss.replace(/[a-zA-Z_]/g, '');

    document.getElementById("data_tableComment").deleteRow(noCommentsss);
    ChangeIds();
    AddCommentListenersToRow();
    SaveCommentSnipettsToLocalStorage();


}

function add_rowComment()
{
    var new_nameFastTextComment=document.getElementById("new_nameFastTextComment").value;
    var new_sourceFastTextComment=document.getElementById("new_sourceFastTextComment").value;

    var tableComment=document.getElementById("data_tableComment");
    var tableComment_len=(tableComment.rows.length)-1;
    var row = tableComment.insertRow(tableComment_len).outerHTML="<tr id='row"+tableComment_len+"' class='rowComment'><td id='nameFastTextComment_row"+tableComment_len+"' class='nameFastTextComment_rowComment'>"+new_nameFastTextComment+"</td><td id='sourceFastTextComment_row"+tableComment_len+"'class='sourceFastTextComment_rowComment'>"+new_sourceFastTextComment+"</td><td><input type='button' id='edit_buttonComment"+tableComment_len+"' value='Edit' class='Commentedit' > <input type='button' id='save_buttonComment"+tableComment_len+"' value='Save' class='Commentsave' > <input type='button' id='delete_button"+tableComment_len+"'  value='Delete' class='Commentdelete'></td></tr>";
    document.getElementById("save_buttonComment"+tableComment_len).style.display="none";
    document.getElementById("new_nameFastTextComment").value="";
    document.getElementById("new_sourceFastTextComment").value="";
    SaveCommentSnipettsToLocalStorage();
    AddCommentListenersToRow();
}

function AddCommentListenersToRow()
{
    function DeleteListeners(el)
    {
        elClone = el.cloneNode(true);
        el.parentNode.replaceChild(elClone, el);

    }


    for(i=0;i<document.getElementsByClassName("Commentedit").length;i++)
    {
        DeleteListeners(document.getElementsByClassName("Commentedit")[i]);

        document.getElementsByClassName("Commentedit")[i].addEventListener('click', function() { edit_rowComment(this.id);});

        DeleteListeners(document.getElementsByClassName("Commentsave")[i]);

        document.getElementsByClassName("Commentsave")[i].addEventListener('click',function() { save_rowComment(this.id);});

        DeleteListeners(document.getElementsByClassName("Commentdelete")[i]);

        document.getElementsByClassName("Commentdelete")[i].addEventListener('click',function() { delete_rowComment(this.id);});

    }

}

var nameFastTextComments = [];
var sourceFastTextComments = [];

function SaveCommentSnipettsToLocalStorage()
{
    (function() {
        var nameFastTextComments = [];
        var sourceFastTextComments = [];
        var table = document.getElementById('data_tableComment');
        var numberOfSnipetts = (table.rows.length) - 2;

        for (i = 1; i <= numberOfSnipetts; i++)
        {
            nameFastTextComments.push(document.getElementById("nameFastTextComment_row"+i).textContent);
            sourceFastTextComments.push(document.getElementById("sourceFastTextComment_row"+i).textContent);
        }
        //Wrzuc tablice do localStorage
        localStorage.setItem("nameFastTextComments", JSON.stringify(nameFastTextComments));
        localStorage.setItem("sourceFastTextComments", JSON.stringify(sourceFastTextComments));
    })();
}

function GetCommentSnipettsFromLocalStorage()
{
    nameFastTextComments = [];
    sourceFastTextComments = [];

    if(JSON.parse(localStorage.getItem("nameFastTextComments")) === null)
    {
        nameFastTextComments.push("Nazwa szybkiego tekstu");
        sourceFastTextComments.push("Treść - XXXXX pozwala na szybkie zaznaczenie, jeżeli zamienisz iksy na jakieś słowo/znak to naciśnięcie klawisza Tab automatycznie zaznaczy drugie użycie XXXXX");
    }
    else
    {
        nameFastTextComments = JSON.parse(localStorage.getItem("nameFastTextComments")); 
        sourceFastTextComments = JSON.parse(localStorage.getItem("sourceFastTextComments"));
    }
}


function ShowCommentSnipetts()
{
    var table = document.getElementById('data_tableComment');
    var numberOfSnipetts = (table.rows.length) - 2;

    if(nameFastTextComments && sourceFastTextComments &&  numberOfSnipetts != nameFastTextComments.length )
    {
        for(i=0;i<nameFastTextComments.length;i++)
            ShowCommentSnipettAsRows(nameFastTextComments[i],sourceFastTextComments[i]);
    }

}

function AddFastTextToComment(idk)
{
    element= document.getElementById('comment');
    idk=idk.replace("FastTextCommentToInsert", "");
    textToInput = sourceFastTextComments[idk];
    if(element.value.indexOf(textToInput) !== -1)
        element.value= element.value.replace(textToInput,"");
    else
        element.value+=textToInput;


    var opis = document.getElementById("comment");
    var textToSelect = "XXXXX";
    var startselect= opis.value.indexOf(textToSelect);
    opis.focus();
    if(startselect !== -1)
    {
        opis.setSelectionRange(startselect,startselect+5);	
    }


    var temp = document.getElementById("comment").value;
    var count = (temp.match(/XXXXX/g) || []).length;
    if(count >=1)
    {
        var OnOff = document.getElementById("MarkNextTextOn");
        OnOff.value=1;

        var body = document.querySelector('body');
        body.addEventListener('keyup', MarkNextText);
    }


}
function MarkNextText(e)
{
    var opis = document.getElementById("comment");
    var OnOff = document.getElementById("MarkNextTextOn");
    var LastText = document.getElementById("MarkNextTextLastText");
    e = e || event;
    if(OnOff.value == 1 && LastText.value !== opis.value && e.keyCode == 9) {
        var opis = document.getElementById("comment");
        var OnOff = document.getElementById("MarkNextTextOn");
        var LastText = document.getElementById("MarkNextTextLastText");
        var textToSelect = "XXXXX";
        var startselect= opis.value.indexOf(textToSelect);
        document.getElementById("comment").focus();
        LastText.value=opis.value;

        if(startselect !== -1)
        {
            document.getElementById("comment").focus();
            document.getElementById("comment").setSelectionRange(startselect,startselect+5);	
        }
    }
    else if(e.keyCode == 9)
    {
        OnOff.value=0;
        LastText.value=opis.value;
    }

}


function ShowCommentSnipettAsRows(new_nameFastTextComment,new_sourceFastTextComment)
{



    var tableComment=document.getElementById("data_tableComment");
    var tableComment_len=(tableComment.rows.length)-1;
    var row = tableComment.insertRow(tableComment_len).outerHTML="<tr id='row"+tableComment_len+"' class='rowComment'><td id='nameFastTextComment_row"+tableComment_len+"' class='nameFastTextComment_rowComment'>"+new_nameFastTextComment+"</td><td id='sourceFastTextComment_row"+tableComment_len+"'class='sourceFastTextComment_rowComment'>"+new_sourceFastTextComment+"</td><td><input type='button' id='edit_buttonComment"+tableComment_len+"' value='Edit' class='Commentedit' > <input type='button' id='save_buttonComment"+tableComment_len+"' value='Save' class='Commentsave' > <input type='button' id='delete_buttonComment"+tableComment_len+"'  value='Delete' class='Commentdelete'></td></tr>";

    document.getElementById("new_nameFastTextComment").value="";
    document.getElementById("new_sourceFastTextComment").value="";
    document.getElementById("save_buttonComment"+tableComment_len).style.display="none";
}


function Import()
{

    var expor = document.getElementById("fastTextcommentImport").value;
    if (expor === null)
    {
        alert("Nie ma czego importować");
        return 0;
    }

    var temp = new Array();
    var nameFastTextCommentsImport = new Array();
    var sourceFastTextCommentsImport = new Array();


    temp = expor.split("<|_*_|>");
    nameFastTextCommentsImport= temp[0].split("<||>");
    sourceFastTextCommentsImport= temp[1].split("<||>");
    numberOfImport = nameFastTextCommentsImport.length;
    var actualnameFastTextComments = new Array();
    var actualsourceFastTextComments = new Array();
    var actualnameFastTextComments = JSON.parse(localStorage.getItem("nameFastTextComments")); 
    var actualsourceFastTextComments = JSON.parse(localStorage.getItem("sourceFastTextComments"));


    for (i = 0; i < numberOfImport-1; i++)
    {
        actualnameFastTextComments.push(nameFastTextCommentsImport[i]);
        actualsourceFastTextComments.push(sourceFastTextCommentsImport[i]);
    }
    localStorage.setItem("nameFastTextComments", JSON.stringify(actualnameFastTextComments));
    localStorage.setItem("sourceFastTextComments", JSON.stringify(actualsourceFastTextComments));
    alert("Udało się!");

}

function Export()
{
    nameFastTextComments = JSON.parse(localStorage.getItem("nameFastTextComments")); 
    sourceFastTextComments = JSON.parse(localStorage.getItem("sourceFastTextComments"));

    var temp1='';
    var temp2='';

    for (i = 0; i < nameFastTextComments.length; i++)
    {
        temp1+=nameFastTextComments[i];
        temp1+="<||>";
        temp2+=sourceFastTextComments[i];
        temp2+="<||>";
    }


    var expor = temp1 +"<|_*_|>" + temp2;
    document.getElementById("fastTextcommentExport").value=expor;
}

function ShowHiddeFastTextComment()
{

    if(!$('#fasttextComment').is(':visible'))
    {
        document.getElementById("fasttextComment").style.display="block"; 
    }
    else
        document.getElementById("fasttextComment").style.display="none";
}

function Run()
{

    GetCommentSnipettsFromLocalStorage();
    var DeclaredFastText='';
    DeclaredFastText+='<div style="border:solid;border-color:AliceBlue;width:100%;float:left;-webkit-box-sizing:border-box;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;" id="fasttextComment"><table border="1" BORDERCOLOR="AliceBlu"><tr align="center">';
    for(i=0;i<nameFastTextComments.length;i++)
    {
        if(i%12===0)
        {
            DeclaredFastText +='</tr><tr align="center">';
        }
        DeclaredFastText+='<td  width="100px"><a id=FastTextCommentToInsert'+i+' class=CommentFastTextToInsertClass title="'+sourceFastTextComments[i].replace(/\n/g, " ")+'">' + nameFastTextComments[i] + '</a> </td>';
    }
    DeclaredFastText+='</tr></table></div>';

    // Miejsce dodania do Jiry
    $('#comment').after(DeclaredFastText);

    var elements= document.getElementsByClassName("CommentFastTextToInsertClass"); 
    for(var i=0;i<elements.length;i++) 
    { 
        elements[i].addEventListener('click', function() {AddFastTextToComment(this.id); }); 
    }

    $("body").append ( ' \
<center><a href="#data_tableComment"><button id="gmShowDlgBtnComment" type="button">Edytuj FastTextComment</button></a></center></br> \
<div id="gmPopupContainerComment">                                               \
<div id="dialog-formComment" title="Szybkie teksty - edytor">\
<p class="CommentvalidateTips"></p> \
<div id="wrapper2"> \
<center><button id="gmCloseDlgBtnComment" type="button">Zakończ edycję.</button>  </center></br> \
<table align="center" cellspacing=2 cellpadding=5 id="data_tableComment" border=1> \
<tr> \
<th>Nazwa</th> \
<th>tekst</th> \
</tr> \
<tr> \
<td><input type="text" id="new_nameFastTextComment"></td> \
<td><textarea id="new_sourceFastTextComment"></textarea></td>\
<td><input type="button" id="addRowComment" class="Commentadd" value="Add Row"></td> \
</tr> \
</table><center></br></br> Export danych:</br>\
<textarea id="fastTextcommentExport"></textarea>\
<br>\
<button id ="ButtonfastTextcommentExport" type=button>Exportuj</button>\
\
</br> Import danych</br>\
<textarea id="fastTextcommentImport"></textarea>\
<br>\
<button id ="ButtonfastTextcommentImport" type=button>Importuj</button></br> \
</center></div> \
</div> \             \
<input type="hidden" id="MarkNextTextOn" value="1">\
<input type="hidden" id="MarkNextTextLastText" value="">   \                                                             \
\
\
\
</div>                                                                    \
' );
    $("#gmPopupContainerComment").hide ();



    $("#gmCloseDlgBtnComment").click ( function () {
        $("#gmPopupContainerComment").hide ();
    } );

    $("#gmShowDlgBtnComment").click ( function () {

        $("#gmPopupContainerComment").show ();
        ShowCommentSnipetts();
        AddCommentListenersToRow();
    } );


    document.getElementById('addRowComment').addEventListener('click', add_rowComment);
    document.getElementById("ButtonfastTextcommentExport").addEventListener('click',Export);
    document.getElementById("ButtonfastTextcommentImport").addEventListener('click',Import);
    document.getElementById('comment-preview_link').addEventListener('click', ShowHiddeFastTextComment);
}

Run();